const User = require("../models/user");

module.exports.checkEmail = (body) => {
	//The mongoDB find() method ALWAYS returns an array
	return User.find({email: body.email}).then(result => {
		if(result.length > 0){ //if a duplicate email is found, result.length is 1. Otherwise, it is 0
			return true; //true means "yes, email exists"
		}else{
			return false; //false means "no, email does not exist"
		}
	})
}

/*ACTIVITY:

	Create a function named register that processes the user's submitted data and creates a new user record in our database. The process flow for this is very similar to creating a new course, with the main differences being the model to use and the data needed to create

	Check your work in Postman by creating a new route called "Register New User" and also check MongoDB Atlas if the user was created in the database.

	Once successfully tested, copy this file (controller/user) and paste it to activities/a1
*/

module.exports.register = (body) => {

	// console.log(body)

	//create a new object called newUser based on our User model. Each of its fields' values will come from the request body
	let newUser = new User({
		firstName: body.name,
		lastName: body.name,
		email:body.email,
		password: body.password,
		mobileNumber: body.mobileNumber,
		enrollments: body.enrollments
		
	})

	//use .save() to save our newCourse object to our database. If saving is NOT successful, an error message will be contained inside of the error parameter passed to .then()

	//If the error parameter has a value, then it will render true in our if statement, and the function will return false

	//If saving is successful, the error parameter will be empty, and thus render false. This will cause our else statement to be run instead and the function will return true
	return newUser.save().then((course, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}